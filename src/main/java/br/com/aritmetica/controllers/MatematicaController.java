package br.com.aritmetica.controllers;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.service.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
//O RestController é para o spring ja saber quando receber um proxy, é por aqui que tem que entrar.
//Ele faz isso automaticamente
//O @Controller é pra mvc, o RestController é pra Restful

@RequestMapping("/matematica")
// <- O Request pega qualquer requisição. Utilizado geralemnte de forma generica no topo da classe.
//Nesse caso, qualquer requisição que venha, tipo localhost:8080/matematica
//Independente se era um get ou um put/post


public class MatematicaController {

    //Testes pra baixo
    @GetMapping("olaMundo")
    //Como tem o get aqui, se eu mandar um get (por exemplo, localhost:8080/matematica/olaMundo), vai cair aqui
    public String OlaMundo() {
        return "Ola mundo. Testinho";
    }

    @PutMapping
    public String OlaPut() {
        return "Agora veio no put";
    }
    //Testes pra cima

    @Autowired
    //pega esse objeto unico que esta instanciado por baixo dos panos (MatematicaService)  <- injeção de dependencia
    //O Spring que intancia automaticamente
    private MatematicaService matematicaService;


    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO) {  //RequestBody quer dizer que é atraves dele (corpo) que vai chegar a requisicao
//O controller que chama o service  <- na linha 37 que começa com o Autowired pra instanciar automaticamente.
        ValidaNumero(entradaDTO);{
            RespostaDTO resposta = matematicaService.soma((entradaDTO));
            return resposta;
        }
    }

    @PutMapping("/subtrai")
    public RespostaDTO subtrai(@RequestBody EntradaDTO entradaDTO) {  //RequestBody quer dizer que é atraves dele (corpo) que vai chegar a requisicao
//O controller que chama o service  <- na linha 37 que começa com o Autowired pra instanciar automaticamente.
        ValidaNumero(entradaDTO);{
            RespostaDTO resposta = matematicaService.subtrai((entradaDTO));
            return resposta;
        }
    }

    @PutMapping("/multiplica")
    public RespostaDTO multiplica(@RequestBody EntradaDTO entradaDTO) {  //RequestBody quer dizer que é atraves dele (corpo) que vai chegar a requisicao
//O controller que chama o service  <- na linha 37 que começa com o Autowired pra instanciar automaticamente.
        ValidaNumero(entradaDTO);{
            RespostaDTO resposta = matematicaService.multiplica((entradaDTO));
            return resposta;
        }
    }

    @PutMapping("/divide")
    public RespostaDTO divide(@RequestBody EntradaDTO entradaDTO) {  //RequestBody quer dizer que é atraves dele (corpo) que vai chegar a requisicao
//O controller que chama o service  <- na linha 37 que começa com o Autowired pra instanciar automaticamente.
        ValidaNumero(entradaDTO);{
            RespostaDTO resposta = matematicaService.divide((entradaDTO));
            return resposta;
        }
    }


    //Classe genérica para fazer as validações que são comuns
    public void ValidaNumero(EntradaDTO entradaDTO ) {
        if (entradaDTO.getNumeros().size() <= 1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 números");
        }
        for (int n : entradaDTO.getNumeros()) {
            if (n < 0) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie números positivos");
            }
        }
    }
    }
