package br.com.aritmetica.service;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service   //Serviço do spring para ser usado ou chamado na classe de controller. Precisa implementar alguns metodos
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO){
        int numero = 0;
        for (int n: entradaDTO.getNumeros()) {
            numero += n;
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtrai(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);
        for (int i = 1; i < entradaDTO.getNumeros().size(); i++) {
            numero -= entradaDTO.getNumeros().get(i);
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO multiplica(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);
        for (int i = 1; i < entradaDTO.getNumeros().size(); i++) {
            numero *= entradaDTO.getNumeros().get(i);
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divide(EntradaDTO entradaDTO){
        int numero = entradaDTO.getNumeros().get(0);
        for (int i = 1; i < entradaDTO.getNumeros().size(); i++) {
            numero /= entradaDTO.getNumeros().get(i);
        }

        RespostaDTO resposta = new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

}
